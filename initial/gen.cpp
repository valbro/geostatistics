#include <iostream>
#include <cstdlib>
#include <fstream>
#include <random>

using std::fstream;


int main(int argc, char** argv) {
	size_t Nx = (argc > 1) ? (atoi(argv[1])) : 100;	
	size_t Known = (argc > 2) ? (atoi(argv[2])) : 1000;	

	std::default_random_engine generator;
	std::normal_distribution<double> normal(0.0, 1.0);
	std::uniform_int_distribution<int> uniform(0,Nx - 1);
	
	size_t x[Known];
	size_t y[Known];
	double f[Nx][Nx];

	for(size_t i = 0; i < Nx; ++i) {
		for(size_t j = 0; j < Nx; ++j) {
			f[i][j] = 0.0;
		}
	}

	for(size_t i = 0; i < Known; ++i) {
		x[i] = uniform(generator);
		y[i] = uniform(generator);
		f[x[i]][y[i]] = normal(generator);
		//std::cout << x[i] << ' ' << y[i] << ' ' << f[x[i]][y[i]] << "\n";
	}

	fstream outfile("in", fstream::out);
	for(size_t i = 0; i < Nx; ++i) {
		for(size_t j = 0; j < Nx; ++j) {
			outfile << i << ' ' << j << ' ' << f[i][j] << "\n";
		}
	}

	outfile.close();
	return 0;
}
